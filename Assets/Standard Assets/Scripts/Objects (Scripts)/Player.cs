using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace RunAndGun
{
	public class Player : Entity
	{
		public string displayName;
		public SortedList<string, BulletPatternEntry> bulletPatternEntriesSortedList = new SortedList<string, BulletPatternEntry>();
		public Transform itemsParent;
		public Item[] items = new Item[0];
		public Weapon[] weapons = new Weapon[0];
		public UseableItem[] useableItems = new UseableItem[0];
		public Transform cooldownIndicatorsParent;
		public Transform bulletSpawnersParent;
		public Animator animator;
		public bool unlocked;
		public Achievement unlockOnCompleteAchievement;
		public GameObject lockedIndicatorGo;
		public GameObject untriedIndicatorGo;
		public CharacterController characterController;
		public float jumpSpeed;
		public Transform headTrs;
		public float lookRate;
		public float drag;
		public float pushOffSpeedMultiplier;
		public float safeDistanceFromPit;
		public float groundCheckDistance;
		public LayerMask whatIsGround;
		[HideInInspector]
		public int id;
		public _Text hpText;
		public _Text moneyText;
		[HideInInspector]
		public uint money;
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>(true);
				return instance;
			}
		}
		Vector3 hitNormal;
		bool canAttack;
		float yVel;
		bool previousJumpInput;
		Vector3 extraVelocity;
		Vector3 lastSafePosition;
		bool previousIsGrounded;
		Vector2 moveInput;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.Awake ();
			hpText.Text = "Health: " + hp;
			if (unlockOnCompleteAchievement == null || unlockOnCompleteAchievement.complete || unlockOnCompleteAchievement.ShouldBeComplete())
			{
				unlocked = true;
				if (lockedIndicatorGo != null)
					lockedIndicatorGo.SetActive(false);
				if (untriedIndicatorGo != null)
					untriedIndicatorGo.SetActive(!SaveAndLoadManager.saveData.triedPlayers.Contains(name));
			}
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			instance = this;
			dead = false;
			items = itemsParent.GetComponentsInChildren<Item>();
			useableItems = itemsParent.GetComponentsInChildren<UseableItem>();
			weapons = itemsParent.GetComponentsInChildren<Weapon>();
			for (int i = 0; i < items.Length; i ++)
			{
				Item item = items[i];
				if (!item.gameObject.activeSelf)
					continue;
				UseableItem useableItem = item as UseableItem;
				if (useableItem != null)
					useableItem.useAction.performed += useableItem.TryToUse;
				item.OnGain ();
			}
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			previousIsGrounded = characterController.isGrounded;
			base.DoUpdate ();
			HandleGravity ();
			HandleJumping ();
			HandleShooting ();
		}

		public override void HandleRotating ()
		{
			Vector2 aimInput = InputManager.AimInput;
			if (aimInput.x != 0)
				trs.Rotate(Vector3.up * aimInput.x * lookRate);
			if (aimInput.y != 0)
				headTrs.Rotate(Vector3.left * aimInput.y * lookRate);
		}

		public override void HandleMoving ()
		{
			moveInput = InputManager.MoveInput;
			Move (moveInput * moveSpeed);
		}
		
		public void ShootBulletPatternEntry (string name)
		{
			if (GameManager.paused)
				return;
			if (!canAttack)
			{
				canAttack = true;
				return;
			}
			bulletPatternEntriesSortedList[name].Shoot ();
		}

		public override void TakeDamage (float amount)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, maxHp);
			hpText.Text = "Health: " + hp;
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public override void Death ()
		{
			_SceneManager.instance.RestartScene ();
		}

		void HandleShooting ()
		{
			for (int i = 0; i < weapons.Length; i ++)
			{
				Weapon weapon = weapons[i];
				if (InputManager.ShootInput)
					weapon.animationEntry.Play ();
			}
		}

		void Move (Vector2 move)
		{
			move = Vector2.ClampMagnitude(move, moveSpeed);
			Vector3 _move = move.XYToXZ();
			_move = trs.rotation * _move;
			extraVelocity = extraVelocity.Shrink(_move * Time.deltaTime);
			// extraVelocity = extraVelocity.Shrink(_move.magnitude * -Vector2.Dot(extraVelocity.normalized, _move.normalized) * Time.deltaTime);
			extraVelocity *= 1f - Time.deltaTime * drag;
			characterController.Move(_move.SetY(yVel) * Time.deltaTime + extraVelocity * Time.deltaTime);
			if (Roguelike.Instance != null)
			{
				Roguelike.instance.DoUpdate ();
				ProceduralLevel.instance.DoUpdate ();
			}
		}

		void HandleGravity ()
		{
			if (characterController.isGrounded)
				yVel = Physics.gravity.y * Time.deltaTime;
			else
			{
				if (previousIsGrounded && yVel < 0 && moveInput != Vector2.zero)
				{
					RaycastHit hit;
					if (Physics.Raycast(trs.position + Vector3.down * characterController.height / 2, Vector3.down, out hit, groundCheckDistance, whatIsGround))
					{
						trs.position += Vector3.down * hit.distance;
						characterController.Move(Physics.gravity * Time.deltaTime);
					}
				}
				yVel += Physics.gravity.y * Time.deltaTime;
			}
		}

		void HandleJumping ()
		{
			bool jumpInput = InputManager.JumpInput;
			if (jumpInput && !previousJumpInput && (characterController.isGrounded || previousIsGrounded || characterController.collisionFlags == CollisionFlags.Sides))
				StartJump ();
			else if (!jumpInput && previousJumpInput && yVel > 0)
				StopJump ();
			previousJumpInput = jumpInput;
		}

		void StartJump ()
		{
			if (!characterController.isGrounded)
				extraVelocity = hitNormal.SetY(0) * jumpSpeed * pushOffSpeedMultiplier;
			yVel += jumpSpeed;
		}

		void StopJump ()
		{
			yVel = 0;
		}

		void OnControllerColliderHit (ControllerColliderHit hitInfo)
		{
			FloorHazard floorHazard = hitInfo.gameObject.GetComponent<FloorHazard>();
			if (floorHazard != null)
			{
				extraVelocity = Vector3.zero;
				trs.position = lastSafePosition;
				TakeDamage (floorHazard.damage);
				return;
			}
			Transform hitTrs = hitInfo.transform;
			MeshFilter hitMeshFilter = hitTrs.GetComponent<MeshFilter>();
			MeshObject meshObject = new MeshObject(hitTrs, hitMeshFilter);
			Shape3D shape = meshObject.ToShape3D(true);
			shape = shape.AddSize(new Vector3(-safeDistanceFromPit, characterController.skinWidth, -safeDistanceFromPit));
			if (shape.Contains(trs.position + Vector3.down * characterController.height / 2))
				lastSafePosition = trs.position;
			hitNormal = hitInfo.normal;
			float dot = Vector2.Dot(hitNormal, extraVelocity.normalized);
			if (dot < 0)
				extraVelocity *= dot + 1;
		}

		public void AddMoney (int amount)
		{
			if (instance != this)
			{
				instance.AddMoney (amount);
				return;
			}
			money = (uint) (money + amount);
			moneyText.Text = "$" + money;
		}
	}
}
#if UNITY_EDITOR
using TMPro;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.TextCore;
using System.Collections.Generic;

namespace RunAndGun
{
	[ExecuteInEditMode]
	public class CopyFontGlyphMetrics : EditorScript
	{
		public TMP_FontAsset copyFromFontAsset;
		public TMP_FontAsset copyToFontAsset;

		public override void Do ()
		{
			for (int i = 0; i < copyFromFontAsset.glyphTable.Count; i ++)
			{
				Glyph glyph = copyFromFontAsset.glyphTable[i];
				Glyph glyph2;
				if (copyToFontAsset.glyphLookupTable.TryGetValue(glyph.index, out glyph2))
					glyph2.metrics = glyph.metrics;
			}
		}
	}
}
#else
namespace RunAndGun
{
	public class CopyFontGlyphMetrics : EditorScript
	{
	}
}
#endif
#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace RunAndGun
{
	[ExecuteInEditMode]
	public class SetFromToRotation : EditorScript
	{
		public Transform trs;
		public Vector3 from;
		public Vector3 to;

		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			trs.rotation = Quaternion.FromToRotation(from, to);
		}
	}
}
#else
namespace RunAndGun
{
	public class SetFromToRotation : EditorScript
	{
	}
}
#endif
using UnityEngine;

public class AnimatorGroup : BehaviourGroup
{
    void OnValidate ()
    {
        for (int i = 0; i < members.Length; i ++)
        {
            Behaviour behaviour = members[i];
            Animator animator = behaviour as Animator;
            if (animator == null)
            {
                animator = behaviour.GetComponent<Animator>();
                if (animator != null)
                    members[i] = animator;
            }
        }
    }
}
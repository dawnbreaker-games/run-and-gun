using UnityEngine;

public class BehaviourGroup : MonoBehaviour
{
	public Behaviour[] members = new Behaviour[0];

	public void SetEnabled (bool enable)
	{
		for (int i = 0; i < members.Length; i ++)
		{
			Behaviour behaviour = members[i];
			behaviour.enabled = enable;
		}
	}

	public void SetActive (bool active)
	{
		for (int i = 0; i < members.Length; i ++)
		{
			Behaviour behaviour = members[i];
			behaviour.gameObject.SetActive(active);
		}
	}
}
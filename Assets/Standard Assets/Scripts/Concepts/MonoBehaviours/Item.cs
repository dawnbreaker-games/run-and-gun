using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RunAndGun
{
	public class Item : MonoBehaviour
	{
		public Transform trs;
		public uint cost;
		public _Text pickupText;
		public Collider pickupCollider;
		public Transform bottomTrs;
		[HideInInspector]
		public bool isInShop;
		PickupUpdater pickupUpdater;

		public virtual void OnGain ()
		{
			trs.SetParent(Player.instance.itemsParent);
			gameObject.SetActive(false);
		}

		public virtual void OnTriggerEnter (Collider other)
		{
			pickupUpdater = new PickupUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(pickupUpdater);
		}

		public virtual void OnTriggerExit (Collider other)
		{
			GameManager.updatables = GameManager.updatables.Remove(pickupUpdater);
		}
		
		public virtual void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(pickupUpdater);
		}

		class PickupUpdater : IUpdatable
		{
			Item item;

			public PickupUpdater (Item item)
			{
				this.item = item;
			}

			public void DoUpdate ()
			{
				RaycastHit hit;
				if (item == null)
					return;
				if (Physics.Raycast(new Ray(Player.instance.headTrs.position, Player.instance.headTrs.forward), out hit, Mathf.Infinity, LayerMask.GetMask(LayerMask.LayerToName(item.pickupCollider.gameObject.layer))) && hit.collider == item.pickupCollider)
				{
					if (item.isInShop)
						item.pickupText.Text = "Press E to buy";
					else
						item.pickupText.Text = "Press E to pickup";
					item.pickupText.gameObject.SetActive(true);
					if (InputManager.InteractInput && (!item.isInShop || Player.instance.money >= item.cost))
					{
						if (item.isInShop)
							Player.instance.AddMoney ((int) -item.cost);
						item.pickupText.gameObject.SetActive(false);
						item.OnGain ();
						GameManager.updatables = GameManager.updatables.Remove(this);
					}
				}
				else
					item.pickupText.gameObject.SetActive(false);
			}
		}
	}
}
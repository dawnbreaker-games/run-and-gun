using System;
using Extensions;

namespace RunAndGun
{
	public class SlowDownKillWall : Item
	{
		public float amount;

		public override void OnGain ()
		{
			base.OnGain ();
			KillWall.instance.moveSpeed -= amount;
			Destroy(gameObject);
		}
	}
}
using Extensions;
using UnityEngine;

namespace RunAndGun
{
	public class AddToAnimatorParameterItem : Item
	{
		public string parameterName;
		public float amount;

		public override void OnGain ()
		{
			base.OnGain ();
			Animator animator = Player.instance.animator;
			animator.SetFloat(parameterName, animator.GetFloat(parameterName) + amount);
		}
	}
}
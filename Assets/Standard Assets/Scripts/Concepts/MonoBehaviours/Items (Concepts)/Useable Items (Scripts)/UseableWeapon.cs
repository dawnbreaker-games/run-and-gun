namespace RunAndGun
{
	public class UseableWeapon : UseableItem
	{
		public BulletPatternEntry bulletPatternEntry;
		public AnimationEntry animationEntry;
		public bool autoShoot;

		public override void OnGain ()
		{
			base.OnGain ();
			pickupCollider.gameObject.SetActive(false);
			gameObject.SetActive(true);
			Player.instance.bulletPatternEntriesSortedList.Add(bulletPatternEntry.name, bulletPatternEntry);
		}

		public override void Use ()
		{
            base.Use ();
			if (autoShoot)
	            bulletPatternEntry.Shoot ();
			if (animationEntry.animator != null)
				animationEntry.Play ();
		}
	}
}
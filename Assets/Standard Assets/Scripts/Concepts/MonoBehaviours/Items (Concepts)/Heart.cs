using Extensions;

namespace RunAndGun
{
	public class Heart : Item
	{
		public override void OnGain ()
		{
			base.OnGain ();
			Player.instance.TakeDamage (-1);
			Player.instance.items = Player.instance.items.Remove(this);
			Destroy(gameObject);
		}
	}
}
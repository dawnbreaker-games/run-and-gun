using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;

namespace RunAndGun
{
	public class UseableItem : Item
	{
		public InputAction useAction;
		public float cooldown;
		[HideInInspector]
		public float cooldownRemaining;
		[HideInInspector]
		public float lastUseTime;
		public bool canHoldUseButton;
		public UseUpdater useUpdater;

		public override void OnGain ()
		{
			base.OnGain ();
			useAction.Enable();
		}

		public void TryToUse (InputAction.CallbackContext context = default(InputAction.CallbackContext))
		{
			if (!GameManager.paused && gameObject.activeInHierarchy && Time.time - lastUseTime >= cooldown)
				Use ();
			if (canHoldUseButton && useUpdater == null)
			{
				useUpdater = new UseUpdater(this);
				GameManager.updatables = GameManager.updatables.Add(useUpdater);
			}
		}

		public virtual void Use ()
		{
			lastUseTime = Time.time;
		}

		public virtual void OnDisable ()
		{
			useAction.Disable();
		}

		public class UseUpdater : IUpdatable
		{
			UseableItem useableItem;

			public UseUpdater (UseableItem useableItem)
			{
				this.useableItem = useableItem;
			}

			public void DoUpdate ()
			{
				if (useableItem.useAction.ReadValue<float>() > InputManager.instance.settings.defaultDeadzoneMin)
					useableItem.TryToUse ();
				else
				{
					useableItem.useUpdater = null;
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}
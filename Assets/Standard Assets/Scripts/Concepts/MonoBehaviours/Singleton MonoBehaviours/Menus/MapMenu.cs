using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace RunAndGun
{
	public class MapMenu : Menu, IUpdatable
	{
		public _Text levelNameText;
		public float cameraLerpRateForRooms;
		public float cameraLerpRateForZones;
		public CameraScript mapCameraScript;
		public Transform currentLevelIndicatorTrs;
#if UNITY_EDITOR
		public bool updateMapCamera;
#endif
		public new static MapMenu instance;
		public new static MapMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<MapMenu>(true);
				return instance;
			}
		}
		public static Level targetLevel;
		static CameraUpdater cameraUpdaterForRooms;
		static CameraUpdater cameraUpdaterForZones;
		static Vector2 moveInput;
		static Vector2? previousMoveInput;
		static Vector2? mousePosition;
		static Vector2? previousMousePosition;
		bool previousOpenPlayerSelectMenuInput;
		bool selectInput;
		bool previousSelectInput = true;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (updateMapCamera)
			{
				updateMapCamera = false;
				Level.instances = FindObjectsOfType<Level>();
				Rect[] levelRects = new Rect[Level.instances.Length];
				for (int i = 0; i < Level.instances.Length; i ++)
				{
					Level level = Level.instances[i];
					levelRects[i] = new Rect((Vector2) level.trs.position - Vector2.one * level.size / 2, Vector2.one * level.size);
				}
				Rect mapRect = levelRects.Combine();
				BossLevel[] bossLevels = FindObjectsOfType<BossLevel>();
				mapRect.size = mapRect.size.SetY(mapRect.size.y / bossLevels.Length);
				mapCameraScript.trs.position = mapRect.center.SetZ(mapCameraScript.trs.position.z);
				mapCameraScript.viewSize = mapRect.size;
			}
		}
#endif

		public override void Open ()
		{
			base.Open ();
			previousMoveInput = null;
			List<Level> levels = new List<Level>(FindObjectsOfType<Level>());
			for (int i = 0; i < levels.Count; i ++)
			{
				Level level = levels[i];
				if (!level.gameObject.activeSelf)
				{
					levels.RemoveAt(i);
					i --;
				}
				else if (level.BestTimeReached > 0)
					level.untriedIndicatorGo.SetActive(false);
			}
			Level.instances = levels.ToArray();
			SetTargetLevel (Level.instance);
			CameraScript.instance.trs.position = targetLevel.trs.position.SetZ(CameraScript.instance.trs.position.z);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public override void Close ()
		{
			base.Close ();
			CameraScript.instance.trs.position = Level.instance.trs.position.SetZ(CameraScript.instance.trs.position.z);
			targetLevel.bestTimeReachedText.gameObject.SetActive(false);
		}

		public void DoUpdate ()
		{
			if (PlayerSelectMenu.Instance.gameObject.activeSelf)
				return;
			bool selectInput = InputManager.SelectInput;
			bool openPlayerSelectMenuInput = InputManager.AbilityInput;
			moveInput = InputManager.MoveInput;
			mousePosition = InputManager.MousePosition;
			HandleSetTargetLevel ();
			if (openPlayerSelectMenuInput && !previousOpenPlayerSelectMenuInput)
			{
				if (!Level.instance.enabled)
					PlayerSelectMenu.instance.Open ();
				else
					GameManager.instance.DisplayNotification ("You can't open the character select menu during gameplay!");
			}
			if ((targetLevel.unlocked || BuildManager.instance.unlockEverything) && !EventSystem.current.IsPointerOverGameObject() && selectInput && !previousSelectInput)
			{
				if (targetLevel != Level.instance)
				{
					Level previousLevel = Level.instance;
					Level.instance = targetLevel;
					SceneManager.sceneLoaded += OnSceneLoaded;
					previousLevel.End ();
				}
				else if (!Level.instance.enabled)
					OnSceneLoaded ();
				else
					Close ();
			}
			previousMoveInput = moveInput;
			previousMousePosition = mousePosition;
			previousOpenPlayerSelectMenuInput = openPlayerSelectMenuInput;
			previousSelectInput = selectInput;
			
		}

	 	void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			instance.Close ();
			Player.Instance.untriedIndicatorGo.SetActive(false);
			Player.instance.trs.position = Level.instance.playerSpawnPoint.position;
			PlayerSelectMenu.instance.currentPlayerNameText.Text = "";
			Player.instance.trs.SetParent(null);
			Level.instance.moveToBeginTextGo.SetActive(true);
			GameManager.updatables = GameManager.updatables.Add(new BeginLevelUpdater());
			SceneManager.sceneLoaded -= OnSceneLoaded;
		}

		void HandleSetTargetLevel ()
		{
			Level newTargetLevel = null;
			if (moveInput != Vector2.zero && previousMoveInput == Vector2.zero)
			{
				for (int i = 0; i < Level.instances.Length; i ++)
				{
					Level level = Level.instances[i];
					Vector2 toLevel = level.trs.position - targetLevel.trs.position;
					float manhattenDistance = toLevel.GetManhattenMagnitude();
					if (manhattenDistance == level.size && Vector2.Angle(toLevel, moveInput) < 45)
					{
						newTargetLevel = level;
						break;
					}
				}
				if (newTargetLevel == null)
				{
					for (int i = 0; i < Level.instances.Length; i ++)
					{
						Level level = Level.instances[i];
						Vector2 toLevel = level.trs.position - targetLevel.trs.position;
						float manhattenDistance = toLevel.GetManhattenMagnitude();
						if (manhattenDistance == level.size * 2 && Vector2.Angle(toLevel, moveInput) < 45)
						{
							newTargetLevel = level;
							break;
						}
					}
					if (newTargetLevel == null)
						newTargetLevel = GetLevelAtWrappedMapMoveInput(moveInput);
				}
			}
			else if (!EventSystem.current.IsPointerOverGameObject() && mousePosition != null && mousePosition != previousMousePosition)
			{
				RenderTexture previousRenderTexture = mapCameraScript.camera.targetTexture;
				mapCameraScript.camera.targetTexture = null;
				for (int i = 0; i < Level.instances.Length; i ++)
				{
					Level level = Level.instances[i];
					if (new Rect((Vector2) level.trs.position - Vector2.one * level.innerSize / 2, Vector2.one * level.innerSize).Contains(mapCameraScript.camera.ScreenToWorldPoint((Vector2) mousePosition)))
					{
						newTargetLevel = level;
						break;
					}
				}
				mapCameraScript.camera.targetTexture = previousRenderTexture;
			}
			if (newTargetLevel != null && newTargetLevel != targetLevel)
				SetTargetLevel (newTargetLevel);
		}

		public void SetTargetLevel (Level level)
		{
			if (level == null)
				return;
			if (!mapCameraScript.viewRect.Contains(level.trs.position))
			{
				Level.instances = FindObjectsOfType<Level>();
				Rect[] levelRects = new Rect[Level.instances.Length];
				for (int i = 0; i < Level.instances.Length; i ++)
				{
					Level _level = Level.instances[i];
					levelRects[i] = new Rect((Vector2) _level.trs.position - Vector2.one * _level.size / 2, Vector2.one * _level.size);
				}
				Rect mapRect = levelRects.Combine();
				BossLevel[] bossLevels = FindObjectsOfType<BossLevel>();
				mapRect.size = mapRect.size.SetY(mapRect.size.y / bossLevels.Length);
				mapCameraScript.viewSize = mapRect.size;
				for (int i = 1; i < bossLevels.Length; i ++)
				{
					if (mapRect.Contains(level.trs.position))
						break;
					mapRect.center += Vector2.up * mapRect.size.y;
				}
				if (cameraUpdaterForZones != null)
					GameManager.updatables = GameManager.updatables.Remove(cameraUpdaterForZones);
				cameraUpdaterForZones = new CameraUpdater(mapCameraScript.trs, mapRect.center, cameraLerpRateForZones);
				GameManager.updatables = GameManager.updatables.Add(cameraUpdaterForZones);
			}
			if (Level.instance == null || !Level.instance.enabled)
				Level.instance = level;
			else if (level != Level.instance)
				level.currentTimeText.Text = "";
			if (level is BossLevel)
				level.bestTimeReachedText.Text = "Best damage: " + string.Format("{0:0.#}", level.BestTimeReached);
			else
				level.bestTimeReachedText.Text = "Best time: " + string.Format("{0:0.#}", level.BestTimeReached);
			targetLevel = level;
			currentLevelIndicatorTrs.position = level.trs.position;
			levelNameText.Text = level.displayName;
			if (cameraUpdaterForRooms != null)
				GameManager.updatables = GameManager.updatables.Remove(cameraUpdaterForRooms);
			cameraUpdaterForRooms = new CameraUpdater(CameraScript.Instance.trs, level.trs.position, cameraLerpRateForRooms);
			GameManager.updatables = GameManager.updatables.Add(cameraUpdaterForRooms);
			AchievementPanel.Instance.DoUpdate ();
			if (LeaderboardMenu.Instance.gameObject.activeSelf && LeaderboardMenu.entryValueType == LeaderboardEntry.ValueType.Time)
				LeaderboardMenu.instance.ReloadLeaderboard ();
		}

		static Level GetLevelAtWrappedMapMoveInput (Vector2 input)
		{
			Level output = null;
			float closestManhattenDistance = 0;
			for (int i = 0; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				Vector2 toLevel = level.trs.position - targetLevel.trs.position;
				float manhattenDistance = toLevel.GetManhattenMagnitude();
				if ((toLevel.x == 0 || toLevel.y == 0) && manhattenDistance > closestManhattenDistance && Vector2.Angle(toLevel, input) > 135)
				{
					output = level;
					closestManhattenDistance = manhattenDistance;
				}
			}
			return output;
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			GameManager.updatables = GameManager.updatables.Remove(cameraUpdaterForRooms);
			GameManager.updatables = GameManager.updatables.Remove(cameraUpdaterForZones);
		}

		class CameraUpdater : IUpdatable
		{
			Transform cameraTrs;
			Vector2 destination;
			float lerpRate;

			public CameraUpdater (Transform cameraTrs, Vector2 destination, float lerpRate)
			{
				this.cameraTrs = cameraTrs;
				this.destination = destination;
				this.lerpRate = lerpRate;
			}

			public void DoUpdate ()
			{
				if (cameraTrs == null)
				{
					GameManager.updatables = GameManager.updatables.Remove(this);
					return;
				}
				float levelDistance = ((Vector2) cameraTrs.position - destination).magnitude;
				float moveAmount = levelDistance * lerpRate * Time.deltaTime;
				if (levelDistance > moveAmount)
					cameraTrs.position = Vector2.Lerp(cameraTrs.position.SetZ(0), destination, lerpRate * Time.deltaTime).SetZ(cameraTrs.position.z);
				else
				{
					cameraTrs.position = destination.SetZ(cameraTrs.position.z);
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}

		class BeginLevelUpdater : IUpdatable
		{
			public void DoUpdate ()
			{
				if (InputManager.MoveInput != Vector2.zero)
				{
					Level.instance.Begin ();
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace RunAndGun
{
	public class PlayerSelectMenu : Menu, IUpdatable
	{
		public _Text currentPlayerNameText;
		public Transform playersParent;
		public Transform currentPlayerIndicatorTrs;
		public new static PlayerSelectMenu instance;
		public new static PlayerSelectMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<PlayerSelectMenu>(true);
				return instance;
			}
		}
		public static Player[] players = new Player[0];
		public static uint currentPlayerIndex;
		static int previousSwitchPlayerInput;
		bool previousSelectInput = true;

		public void Init ()
		{
			players = playersParent.GetComponentsInChildren<Player>();
			SetCurrentPlayer (players[currentPlayerIndex]);
		}

		public override void Open ()
		{
			base.Open ();
			MapMenu.instance.levelNameText.gameObject.SetActive(false);
			SetCurrentPlayer (players[currentPlayerIndex]);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public override void Close ()
		{
			MapMenu.instance.levelNameText.gameObject.SetActive(true);
			gameObject.SetActive(false);
			GameManager.updatables = GameManager.updatables.Remove(this);
			currentPlayerNameText.Text = "";
		}

		public void DoUpdate ()
		{
			bool selectInput = InputManager.SelectInput;
			Vector2 movementInput = InputManager.MoveInput;
			if (movementInput.x != 0 && previousSwitchPlayerInput == 0)
			{
				if (movementInput.x < 0)
				{
					if (currentPlayerIndex > 0)
						currentPlayerIndex --;
					else
						currentPlayerIndex = (uint) players.Length - 1;
				}
				else
				{
					if (currentPlayerIndex < players.Length - 1)
						currentPlayerIndex ++;
					else
						currentPlayerIndex = 0;
				}
				SetCurrentPlayer (players[currentPlayerIndex]);
			}
			if ((Player.instance.unlocked || BuildManager.instance.unlockEverything) && !EventSystem.current.IsPointerOverGameObject() && selectInput && !previousSelectInput)
				Close ();
			previousSwitchPlayerInput = MathfExtensions.Sign(movementInput.x);
			previousSelectInput = selectInput;
		}

		public void SetCurrentPlayer (Player player)
		{
			Player.instance = player;
			currentPlayerIndicatorTrs.position = player.trs.position;
			string text = player.displayName;
			if (player.unlockOnCompleteAchievement != null)
			{
				text += "\n";
				if (player.unlockOnCompleteAchievement.complete)
					text += "(Done) ";
				text += player.unlockOnCompleteAchievement.displayName;
			}
			currentPlayerNameText.Text = text;
		}
	}
}
using System;
using UnityEngine;

namespace RunAndGun
{
	[Serializable]
	public class MultilineString
	{
		[Multiline]
		public string value;
		public virtual string Value
		{
			get
			{
				return value;
			}
			set
			{
				this.value = value;
			}
		}
	}
}
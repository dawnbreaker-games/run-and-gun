using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class BidirectionalMoveableAutoResizableList2D<T> : BidirectionalMoveableAutoResizableList<BidirectionalMoveableAutoResizableList<T>>
{
	public new T this[int xIndex, int yIndex]
	{
		get
		{
			return this[xIndex][yIndex];
		}
		set
		{
			if (autoResizeWhenGetAndSetElements)
			{
				if (xIndex - indexRange.min >= values.Count)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, xIndex), Mathf.Max(indexRange.max, xIndex));
					Add (new BidirectionalMoveableAutoResizableList<T>(indexRange), true);
				}
				else if (xIndex - indexRange.min < 0)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, xIndex), Mathf.Max(indexRange.max, xIndex));
					Add (new BidirectionalMoveableAutoResizableList<T>(indexRange), false);
				}
			}
			this[xIndex][yIndex] = value;
		}
	}

	public BidirectionalMoveableAutoResizableList2D () : base ()
	{
	}

	public BidirectionalMoveableAutoResizableList2D (Vector2Int minIndex, Vector2Int maxIndex, bool autoResizeWhenGetAndSetElements = true)
	{
		indexRange = new IntRange(minIndex.x, maxIndex.x);
		this.autoResizeWhenGetAndSetElements = autoResizeWhenGetAndSetElements;
		for (int x = minIndex.x; x <= maxIndex.x; x ++)
		{
			for (int y = minIndex.y; y <= maxIndex.y; y ++)
				this[x] = new BidirectionalMoveableAutoResizableList<T>(new IntRange(minIndex.y, maxIndex.y));
		}
	}

	public void Insert (Vector2Int index, T element, bool toXEnd, bool toYEnd)
	{
		BidirectionalMoveableAutoResizableList<T> list = this[index.x];
		list.Insert(index.y, element, toYEnd);
		if (Count > 1)
		{
			if (toXEnd)
				indexRange.max ++;
			else
				indexRange.min --;
		}
	}

	public bool Remove (T element, bool toXEnd, bool toYEnd)
	{
		for (int i = indexRange.min; i <= indexRange.max; i ++)
		{
			BidirectionalMoveableAutoResizableList<T> list = this[i];
			if (list.Remove(element, toYEnd))
			{
				if (toXEnd)
					indexRange.max --;
				else
					indexRange.min ++;
				return true;
			}
		}
		return false;
	}

	public void RemoveAt (Vector2Int index, bool toXEnd, bool toYEnd)
	{
		BidirectionalMoveableAutoResizableList<T> list = this[index.x];
		list.RemoveAt(index.x, toYEnd);
		if (toXEnd)
			indexRange.max --;
		else
			indexRange.min ++;
	}

	public void Add (T element, bool toXEnd, bool toYEnd)
	{
		if (toXEnd)
		{
			BidirectionalMoveableAutoResizableList<T> list = this[Count - 1];
			for (int i = list.indexRange.min; i <= list.indexRange.max; i ++)
				list.Add(default(T), toYEnd);
			Add(list);
			if (Count > 1)
				indexRange.max ++;
		}
		else
		{
			BidirectionalMoveableAutoResizableList<T> list = this[0];
			for (int i = indexRange.min; i <= indexRange.max; i ++)
				list.Add(default(T), toYEnd);
			Insert(0, list);
			if (Count > 1)
				indexRange.min --;
		}
	}
}
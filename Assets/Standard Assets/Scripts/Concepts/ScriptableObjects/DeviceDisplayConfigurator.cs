﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Device Display Configurator", menuName = "Scriptable Objects/Device Display Configurator", order = 1)]
public class DeviceDisplayConfigurator : ScriptableObject
{
	public List<DeviceSet> deviceSets = new List<DeviceSet>();
	static Color DEFAULT_DISPLAY_COLOR = Color.white;
	
	public DeviceSet? GetCurrentDeviceSet ()
	{
		string currentDeviceRawPath = InputSystem.devices[0].ToString();
		for (int i = 0; i < deviceSets.Count; i ++)
		{
			DeviceSet deviceSet = deviceSets[i];            
			if (deviceSet.rawPath == currentDeviceRawPath)
				return deviceSet;
		}
		return null;
	}

	public string GetDeviceName ()
	{
		DeviceSet? deviceSet = GetCurrentDeviceSet();
		if (deviceSet != null)
			return ((DeviceSet) deviceSet).displaySettings.name;
		else
			return InputSystem.devices[0].ToString();
	}

	public Sprite GetDeviceBindingIcon (string inputBinding)
	{
		DeviceSet? deviceSet = GetCurrentDeviceSet();
		if (deviceSet != null)
		{
			DeviceDisplaySettings displaySettings = ((DeviceSet) deviceSet).displaySettings;
			if (inputBinding == "Button North")
				return displaySettings.buttonNorthIcon;  
			if (inputBinding == "Button South")
				return displaySettings.buttonSouthIcon;
			if (inputBinding == "Button West")
				return displaySettings.buttonWestIcon;
			if (inputBinding == "Button East")
				return displaySettings.buttonEastIcon;
			if (inputBinding == "Right Shoulder")
				return displaySettings.triggerRightFrontIcon;
			if (inputBinding == "Right Trigger")
				return displaySettings.triggerRightBackIcon;
			if (inputBinding == "rightTriggerButton")
				return displaySettings.triggerRightBackIcon;
			if (inputBinding == "Left Shoulder")
				return displaySettings.triggerLeftFrontIcon;
			if (inputBinding == "Left Trigger")
				return displaySettings.triggerLeftBackIcon;
			if (inputBinding == "leftTriggerButton")
				return displaySettings.triggerLeftBackIcon;
			else
			{
				for (int i = 0; i < displaySettings.customInputContextEntries.Count; i ++)
				{
					CustomInputContextEntry customInputContextEntry = displaySettings.customInputContextEntries[i];
					if (customInputContextEntry.context == inputBinding)
						return customInputContextEntry.sprite;
				}
			}
		}
		return null;
	}

	[Serializable]
	public struct DeviceSet
	{
		public string rawPath;
		public DeviceDisplaySettings displaySettings;
	}
}
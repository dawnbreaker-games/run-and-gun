// using System;
// using Extensions;
// using UnityEngine;

// [Serializable]
// public class Box3D : Shape3D
// {
// 	public Vector3 center;
// 	public Vector3 size;
// 	public Vector3 rotation;

// 	public Box3D (Vector3 center, Vector3 size, Vector3 rotation)
// 	{
// 		this.center = center;
// 		this.size = size;
// 		this.rotation = rotation;
// 		Bounds bounds = new Bounds(center, size);
// 		faces = FromBounds(bounds).Rotate(center, Quaternion.Euler(rotation)).faces;
// 		SetEdgesFromFaces ();
// 		SetCornersFromEdges ();
// 	}

// 	public Vector3 GetRandomPoint ()
// 	{
// 		Bounds bounds = new Bounds(center, size);
// 		return Quaternion.Euler(rotation) * (bounds.RandomPoint() - center) + center;
// 	}
// }
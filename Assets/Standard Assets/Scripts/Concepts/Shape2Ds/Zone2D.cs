using System;
using Extensions;
using UnityEngine;

[Serializable]
public class Zone2D : Shape2D
{
	public Collider2D collider;
	public Transform[] transforms = new Transform[0];
	public Type type;

	public Zone2D (Transform[] transforms)
	{
		type = Type.Transforms;
		this.transforms = transforms;
		corners = new Vector2[transforms.Length];
		for (int i = 0; i < transforms.Length; i ++)
		{
			Transform trs = transforms[i];
			corners[i] = trs.position;
		}
		SetEdgesOfPolygon ();
	}

	public Zone2D (Collider2D collider)
	{
		type = Type.ColliderCorners;
		this.collider = collider;
		EdgeCollider2D edgeCollider = collider as EdgeCollider2D;
		if (edgeCollider != null)
			corners = edgeCollider.points;
		else
		{
			PolygonCollider2D polygonCollider = collider as PolygonCollider2D;
			if (polygonCollider != null)
				corners = polygonCollider.points;
			else
			{
				BoxCollider2D boxCollider = collider as BoxCollider2D;
				if (boxCollider != null)
					corners = boxCollider.bounds.ToRect().GetCorners();
			}
		}
		SetEdgesOfPolygon ();
	}

	public enum Type
	{
		ColliderCorners,
		Transforms
	}
}